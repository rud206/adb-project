import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class TransactionManager {
	private String outputFile = "";
	private Map<Integer, Site> sites;
	private Map<String, Transaction> currentTransactions;
	private int time = 0;
	public Map<String, ArrayList<Operation>> waitList;
	
	//Constructor
	public TransactionManager(String op){
		this.outputFile=op;
		sites = new HashMap<Integer, Site>();
		for (int i = 1; i <= Constants.SITES; i++) {
			Site site = new Site(i);
			sites.put(i, site);
		}
		currentTransactions = new HashMap<String, Transaction>();
		waitList = new HashMap<String, ArrayList<Operation>>();
	}
	
	
	//Function that takes line by line input and takes initiates action
	public void execute(String line){
		time++;
		String[] operation = line.split("; ");
		ArrayList<String> endTransactionList = new ArrayList<String>();
		for (String op : operation) {
			System.out.println(op);
			if(op.startsWith("dump()")){
				dump();
			}else if(op.startsWith("dump(x")){
				int index=Integer.parseInt(op.substring(6, op.length()-1));
				dumpx(index);
			}else if(op.startsWith("dump(")){
				int index=Integer.parseInt(op.substring(5, op.length()-1));
				dumpI(index);
			}else if(op.startsWith("begin(")){
				beginTransaction(op.substring(6, op.length() - 1), Constants.RW);
			}else if(op.startsWith("beginRO(")){
				beginTransaction(op.substring(8, op.length() - 1), Constants.RO);
			}else if(op.startsWith("R(")){
				String[] t = op.substring(2, op.length() - 1).split(",");
				readValue(t[0],
						Integer.parseInt(t[1].substring(t[1].indexOf("x") + 1)));
			}else if(op.startsWith("W(")){
				String[] t = op.substring(2, op.length() - 1).split(",");
				writeValue(
						t[0],
						Integer.parseInt(t[1].substring(t[1].indexOf("x") + 1)),
						Integer.parseInt(t[2]));
			} else if (op.startsWith("end(")) {
				//endTransaction(op.substring(4, op.length() - 1));
				endTransactionList.add(op.substring(4, op.length() - 1));
			}else if(op.startsWith("fail(")){
				failSite(op.substring(5, op.length() - 1));
			} else if (op.startsWith("recover(")) {
				recoverSite(op.substring(8, op.length() - 1));
			}
		}
		for(int i = 0 ; i < endTransactionList.size(); i++){
			endTransaction(endTransactionList.get(i));
		}
	}

	
	//prints the values at site with index i
	public void dumpI(int index){
		if(sites.containsKey(index)){
			System.out.print("Site " + index+"\n");
			System.out.print(sites.get(index).toString());//toString() already has \n at the end
		}
		System.out.print("\n");
	}
	
	//prints value of variable i across all sites
	public void dumpx(int index){
		for(int i = 1 ; i <= Constants.SITES; i++){
			ArrayList<Variable> var=(ArrayList<Variable>) sites.get(i).getVariables();
			for(int j = 0 ; j < var.size(); j++){
				if(var.get(j).getID()==index){
					System.out.print("Site " + i+"\n");
					System.out.print(var.get(j).toString());//toString() already has \n at the end
				}
			}
		}
		System.out.print("\n");
	}
	
	
	//prints values of all variables on all sites
	public void dump(){
		for(int i = 1 ; i <= Constants.SITES; i++){
			System.out.print("Site " + i + "\n");
			System.out.print(sites.get(i).toString());//toString() already has \n at the end
		}
		System.out.print("\n");
	}
	
	
	//performs the begin operation for both Read-Write and Read-Only Transactions
	private void beginTransaction(String transactionID, int typeOfTransaction) {
		if (!currentTransactions.containsKey(transactionID)) {
			Transaction transaction = new Transaction(transactionID, time,
					typeOfTransaction);
			currentTransactions.put(transactionID, transaction);
		}
	}

	
	//performs the fail(siteID) operation 
	private void failSite(String str) {
		int siteID = Integer.parseInt(str);
		if (this.sites.containsKey(siteID)) {
			Site s = (Site) sites.get(siteID);
			s.fail();
		}else{
			//invalid site id
			System.out.println("SUCH A SITE DOES NOT EXIST (INVALID OPERATION fail("+str+")");
		}
	}

	
	
	private void recoverSite(String siteID) {	
		int site = Integer.parseInt(siteID);
		if(this.sites.containsKey(site)){
			this.sites.get(site).recover();
		}
		else{
			//No such site
			System.out.println("INVALID operation recover("+siteID+")");
		}
	}
	
	
	
}