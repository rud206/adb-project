import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class TransactionManager {
	private String outputFile = "";
	private Map<Integer, Site> sites;
	private Map<String, Transaction> currentTransactions;
	private int time = 0;
	public Map<String, ArrayList<Operation>> waitList;
	
	//Constructor
	public TransactionManager(String op){
		this.outputFile=op;
		sites = new HashMap<Integer, Site>();
		for (int i = 1; i <= Constants.SITES; i++) {
			Site site = new Site(i);
			sites.put(i, site);
		}
		currentTransactions = new HashMap<String, Transaction>();
		waitList = new HashMap<String, ArrayList<Operation>>();
	}
	
	
	//Function that takes line by line input and takes initiates action
	public void execute(String line){
		time++;
		String[] operation = line.split("; ");
		ArrayList<String> endTransactionList = new ArrayList<String>();
		for (String op : operation) {
			System.out.println(op);
			if(op.startsWith("dump()")){
				dump();
			}else if(op.startsWith("dump(x")){
				int index=Integer.parseInt(op.substring(6, op.length()-1));
				dumpx(index);
			}else if(op.startsWith("dump(")){
				int index=Integer.parseInt(op.substring(5, op.length()-1));
				dumpI(index);
			}else if(op.startsWith("begin(")){
				beginTransaction(op.substring(6, op.length() - 1), Constants.RW);
			}else if(op.startsWith("beginRO(")){
				beginTransaction(op.substring(8, op.length() - 1), Constants.RO);
			}else if(op.startsWith("R(")){
				String[] t = op.substring(2, op.length() - 1).split(",");
				readValue(t[0],
						Integer.parseInt(t[1].substring(t[1].indexOf("x") + 1)));
			}else if(op.startsWith("W(")){
				String[] t = op.substring(2, op.length() - 1).split(",");
				writeValue(
						t[0],
						Integer.parseInt(t[1].substring(t[1].indexOf("x") + 1)),
						Integer.parseInt(t[2]));
			} else if (op.startsWith("end(")) {
				//endTransaction(op.substring(4, op.length() - 1));
				endTransactionList.add(op.substring(4, op.length() - 1));
			}else if(op.startsWith("fail(")){
				failSite(op.substring(5, op.length() - 1));
			} else if (op.startsWith("recover(")) {
				recoverSite(op.substring(8, op.length() - 1));
			}
		}
		for(int i = 0 ; i < endTransactionList.size(); i++){
			endTransaction(endTransactionList.get(i));
		}
	}

	
	//prints the values at site with index i
	public void dumpI(int index){
		if(sites.containsKey(index)){
			System.out.print("Site " + index+"\n");
			System.out.print(sites.get(index).toString());//toString() already has \n at the end
		}
		System.out.print("\n");
	}
	
	//prints value of variable i across all sites
	public void dumpx(int index){
		for(int i = 1 ; i <= Constants.SITES; i++){
			ArrayList<Variable> var=(ArrayList<Variable>) sites.get(i).getVariables();
			for(int j = 0 ; j < var.size(); j++){
				if(var.get(j).getID()==index){
					System.out.print("Site " + i+"\n");
					System.out.print(var.get(j).toString());//toString() already has \n at the end
				}
			}
		}
		System.out.print("\n");
	}
	
	
	//prints values of all variables on all sites
	public void dump(){
		for(int i = 1 ; i <= Constants.SITES; i++){
			System.out.print("Site " + i + "\n");
			System.out.print(sites.get(i).toString());//toString() already has \n at the end
		}
		System.out.print("\n");
	}
	
	
	//performs the begin operation for both Read-Write and Read-Only Transactions
	private void beginTransaction(String transactionID, int typeOfTransaction) {
		if (!currentTransactions.containsKey(transactionID)) {
			Transaction transaction = new Transaction(transactionID, time,
					typeOfTransaction);
			currentTransactions.put(transactionID, transaction);
		}
	}

	
	//performs the fail(siteID) operation 
	private void failSite(String str) {
		int siteID = Integer.parseInt(str);
		if (this.sites.containsKey(siteID)) {
			Site s = (Site) sites.get(siteID);
			s.fail();
		}else{
			//invalid site id
			System.out.println("SUCH A SITE DOES NOT EXIST (INVALID OPERATION fail("+str+")");
		}
	}

	
	//performs the recover(siteID) operation
	private void recoverSite(String siteID) {	
		int site = Integer.parseInt(siteID);
		if(this.sites.containsKey(site)){
			this.sites.get(site).recover();
		}
		else{
			//No such site
			System.out.println("INVALID operation recover("+siteID+")");
		}
	}
	
	
	//counts the number of UP Sites containing Variable variableID
	private int countUpSitesContainingVariable(int variableID){
		int answer=0;
		for(int i = 1; i<=Constants.SITES; i++){
			if(!this.sites.get(i).isDown()){
				ArrayList<Variable> vars = (ArrayList<Variable>) this.sites.get(i).getVariables();
				for(int j = 0 ; j < vars.size(); j++){
					if(vars.get(j).getID()==variableID){
						answer++;
					}
				}
			}
		}
		return answer;
	}
	
	
	//returns an ArrayList<Lock> of all the locks on Variable variableID across all sites
	private ArrayList<Lock> getAllLocksForVariableFromAllSites(int variableID){
		ArrayList<Lock> ans = new ArrayList<>();
		for(int i = 1 ; i <= Constants.SITES; i++){
			ArrayList<Lock> lockForThisSite = this.sites.get(i).lockInfo.getAllLocksForVariable(variableID);
			for(int j = 0 ; j < lockForThisSite.size(); j++){
				ans.add(lockForThisSite.get(j));
			}
		}
		return ans;
	}
	
	
	//returns the number of sites that are up
	private int numberOfUpSites(){
		int count = 0 ;
		for(int i = 1 ; i <= Constants.SITES; i++){
			if(!this.sites.get(i).isDown()){
				count++;
			}
		}
		return count;
	}

	
	//returns the total number of locks on Variable variableID across all sites
	public int totalNumberOfLocksOnAllSitesForVariable(int variableID){
		int answer=0;
		for(int i = 1 ; i <= Constants.SITES; i++){
			answer+=sites.get(i).lockInfo.getAllLocksForVariable(variableID).size();
		}
		return answer;
	}
	
	
	//returns true if there is an older transaction which holds lock on Variable variableID
	//across any site
	private boolean isThereAnOlderTransactionWithLockOnVariable(int tStartTime,int variableID){
		for(int i = 1 ; i <= Constants.SITES; i++){
			Site tempSite=this.sites.get(i);
			ArrayList<Lock> lockForVariable=tempSite.lockInfo.getAllLocksForVariable(variableID);
			for(int j = 0 ; j < lockForVariable.size(); j++){
				String currentTID = lockForVariable.get(j).getTransactionId();
				int currentStartTime = this.currentTransactions.get(currentTID).getStartTime();
				if(currentStartTime < tStartTime){
					return true;
				}
			}
		}
		return false;
	}
	
	
	//returns number of sites that are UP and which contain Variable variableID
	private int upSiteCountContainingVariable(int variableID){
		int answer=0;
		for(int i = 1 ; i <= Constants.SITES; i++){
			if(!this.sites.get(i).isDown()){
				//site is not down
				ArrayList<Variable> vList = (ArrayList<Variable>) this.sites.get(i).getVariables();
				for(int j = 0; j < vList.size(); j++){
					if(vList.get(j).getID()==variableID){
						answer++;
					}
				}
			}else{
				//site is down
			}
		}
		return answer;
	}

	
	//This method acts as intermediary between execute and read()
	//calls the read() with appropriate value for typeOfTransaction
	private void readValue(String transactionID, int variable) {
		if (currentTransactions.containsKey(transactionID)) {
			Transaction t = (Transaction) currentTransactions
					.get(transactionID);
			if (t.getTypeOfTransaction() == Constants.RO) {
				read(transactionID, variable, Constants.RO);
			} else {
				read(transactionID, variable, -1);
			}
		}
	}
	
	
	//returns true if the Transaction already has a read lock on the variable
	private boolean doesThisTransactionAlreadyHaveReadLockOnVariable(Transaction trans,int variableID){
		ArrayList<Lock> locked = trans.getAlreadyLockedList();
		for(int i = 0 ; i < locked.size(); i++){
			if(locked.get(i).getLockType().equals("1") && locked.get(i).getVariableID()==variableID){
				return true;
			}
		}
		return false;
	}
	
	
	//aborts the Transaction transactionID
	private void abort(String transactionID) {
		if (currentTransactions.containsKey(transactionID)) {
			Transaction t = (Transaction) currentTransactions.get(transactionID);
			ArrayList<Operation> ops = (ArrayList<Operation>) t.getOperations();
			for(int i = 0 ; i < ops.size(); i++){
				if(ops.get(i).getOperationType()==Constants.OP_WRITE){
					//This operation is a write operation
					//The variable that was written to
					//It Value has to be set to CurrentValue
					//across all sites that are up
					int variableID=ops.get(i).getVariableIndex();
					for(int j = 1; j<=Constants.SITES;j++){
						if(!this.sites.get(j).isDown()){
							ArrayList<Variable> tempVar = (ArrayList<Variable>) this.sites.get(j).getVariables();
							for(int p = 0 ; p < tempVar.size(); p++){
								if(tempVar.get(p).getID()==variableID){
									this.sites.get(j).setCurrValToVal(variableID);
								}
							}
						}else{//site is down
							//if site is down do not do anything
						}
					}
				}else{
					//for read operation
					//there is no change that has to be made to the database for read operation
				}
			}
						
			for(int i = 1; i <= Constants.SITES && !this.sites.get(i).isDown(); i++){
				this.sites.get(i).lockInfo.removeLockWithTransactionID(transactionID);
			}
			
			//notify the transaction that waits for this transaction to end/abort that this transaction has now aborted
			this.notifyWaitingTransaction(t.getTransactionID());
			
		}else{
			//transaction ID is invalid
			System.out.println(transactionID+" IS INVALID");
		}
		
	}
	
	
	//inserts Transaction transactionID's Operation op into the waitList
	private void insertToWaitList(Operation op, String transactionID) {
		ArrayList<Operation> ops;
		if (waitList.containsKey(transactionID)) {
			ops = (ArrayList<Operation>) waitList.get(transactionID);
			ops.add(op);
		} else {
			ops = new ArrayList<Operation>();
			ops.add(op);
			waitList.put(transactionID, ops);
		}
	}
	
	
	
	
}